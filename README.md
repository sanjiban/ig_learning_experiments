# README #

This is a repository for conducting experiments on learning to gather information.

## Setup ##
1. Create a catkin workspace.
2. Clone this repository.
3. Copy the .rosinstall file from this repository to your catkin workspace.
4. do wstool up
