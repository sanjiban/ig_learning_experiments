/* Copyright 2015 Sanjiban Choudhury
 * example_test_online_policy.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning/state.h"
#include "ig_learning/state_utils.h"
#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_learning_experiments/io_utils.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"
#include "ig_learning/cost_functions/constant_cost_function.h"
#include "ig_learning/cost_functions/motion_cost_function.h"
#include "ig_learning/clairvoyant_oracles/one_step_reward_clairvoyant_oracle.h"
#include "ig_learning/feature_extractor/ig_feature_extractor.h"
#include "ig_learning/feature_extractor/last_vertex_pose_feature_extractor.h"
#include "ig_learning/feature_extractor/motion_length_feature_extractor.h"

#include "ig_learning/cs_classification/rdf_regression_cs_classification.h"
#include "ig_learning/imitation_learning/forward_training.h"
#include "ig_learning/online_policies/learnt_forward_training.h"
#include "ig_learning/online_policies/learnt_aggrevate.h"
#include "ig_learning/online_policies/ig_heuristic_policy.h"
#include "ig_learning/online_policies/ig_heuristic_penalized_motion_policy.h"

using namespace ig_learning;

namespace io = io_utils;
namespace su = state_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_test_online_policy");
  ros::NodeHandle n("~");

  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("output_pcl", 1);
  ros::Duration(1.0).sleep();

  // Load Stuff
  std::vector< std::pair<State, WorldMap> > test_dataset;
  int total_timesteps = 0;
  {
    double model_res;
    ros_tools::getExpParam(model_res,"model_res", n);
    std::vector<WorldMap> world_map_set;
    std::string test_foldername;
    ros_tools::getExpParam(test_foldername,"test_foldername", n);
    int num_test;
    ros_tools::getExpParam(num_test,"num_test", n);
    io::LoadWorldMapSet(test_foldername, num_test, model_res, world_map_set);
    ros_tools::getExpParam(total_timesteps,"total_timesteps", n);

    for (auto it : world_map_set) {
      State state;
      InitializeState(n, state);
      test_dataset.emplace_back(state, it);
    }
  }

  //Load problem setup
  StateTransitionPtr state_belief_tr_fn;
  ObjectiveFunctionPtr obj_fn;
  CostFunctionPtr cost_fn;
  double cost_budget = 0;

  int option_problem_setup;
  ros_tools::getExpParam(option_problem_setup,"option_problem_setup", n);
  switch (option_problem_setup) {
    case 1: {
      state_belief_tr_fn.reset(new PclLookupStateBeliefTransition());
      boost::static_pointer_cast<PclLookupStateBeliefTransition>(state_belief_tr_fn)->Initialize(n);
      obj_fn.reset( new PclVoxelMatchObjectiveFunction());
      cost_fn.reset( new ConstantCostFunction(0));
      cost_budget = std::numeric_limits<double>::infinity();
      break;
    }
    case 2: {
      state_belief_tr_fn.reset(new PclLookupStateBeliefTransition());
      boost::static_pointer_cast<PclLookupStateBeliefTransition>(state_belief_tr_fn)->Initialize(n);
      obj_fn.reset( new PclVoxelMatchObjectiveFunction());
      cost_fn.reset( new MotionCostFunction());
      ros_tools::getExpParam(cost_budget,"cost_budget", n);
      break;
    }
  }


  // Create policy
  int option_policy;
  ros_tools::getExpParam(option_policy,"option_policy", n);
  OnlinePolicyPtr policy;

  if (option_policy == 1) {
    ConcatenateFeatureExtractorPtr feature_fn(new ConcatenateFeatureExtractor());
    {
      IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
      ig_feature_fn->Initialize(n);
      feature_fn->AddFeatureExtractorSet(ig_feature_fn);
      LastVertexPoseFeatureExtractorPtr pose_feature_fn(new LastVertexPoseFeatureExtractor(LastVertexPoseFeatureExtractor::REL_POS_ONLY));
      feature_fn->AddFeatureExtractorSet(pose_feature_fn);
    }

    RDFRegressionPtr cs_class_fn(new RDFRegression());
    cs_class_fn->Initialize(20);

    std::string predictor_foldername;
    ros_tools::getExpParam(predictor_foldername, "predictor_foldername", n);
    std::vector<CSClassificationPtr> model_set;
    for (unsigned int i = 0; i < total_timesteps; i++) {
      std::string predictor_filename = predictor_foldername + std::to_string(i)+".pred";
      CSClassificationPtr model(cs_class_fn->Clone());
      model->Load(predictor_filename);
      model_set.push_back(model);
    }

    policy.reset(new LearntForwardTraining(feature_fn, model_set));
  } else if(option_policy == 2) {
    IGHeuristicPolicyPtr ig_pol(new IGHeuristicPolicy());
    if(!ig_pol->Initialize(n)) {
      ROS_ERROR_STREAM("Couldnt load ig pol");
      return EXIT_FAILURE;
    }
    policy = ig_pol;
  } else  if (option_policy == 3) {
    ConcatenateFeatureExtractorPtr feature_fn(new ConcatenateFeatureExtractor());
    {
      IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
      ig_feature_fn->Initialize(n);
      feature_fn->AddFeatureExtractorSet(ig_feature_fn);
    }

    RDFRegressionPtr cs_class_fn(new RDFRegression());
    cs_class_fn->Initialize(20);

    std::string predictor_foldername;
    ros_tools::getExpParam(predictor_foldername, "predictor_foldername", n);
    std::vector<CSClassificationPtr> model_set;
    for (unsigned int i = 0; i < total_timesteps; i++) {
      std::string predictor_filename = predictor_foldername + std::to_string(i)+".pred";
      CSClassificationPtr model(cs_class_fn->Clone());
      model->Load(predictor_filename);
      model_set.push_back(model);
    }

    policy.reset(new LearntForwardTraining(feature_fn, model_set));
  } else if(option_policy == 4) {
    IGHeuristicMotionPolicyPtr ig_pol(new IGHeuristicMotionPolicy());
    if(!ig_pol->Initialize(n)) {
      ROS_ERROR_STREAM("Couldnt load ig pol");
      return EXIT_FAILURE;
    }
    policy = ig_pol;
  } else if (option_policy == 5) {
    ConcatenateFeatureExtractorPtr feature_fn(new ConcatenateFeatureExtractor());
    {
      IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
      ig_feature_fn->Initialize(n);
      feature_fn->AddFeatureExtractorSet(ig_feature_fn);
      LastVertexPoseFeatureExtractorPtr pose_feature_fn(new LastVertexPoseFeatureExtractor(LastVertexPoseFeatureExtractor::ABS_POS_ONLY));
      feature_fn->AddFeatureExtractorSet(pose_feature_fn);
      MotionLengthFeatureExtractorPtr motion_feature_fn(new MotionLengthFeatureExtractor());
      feature_fn->AddFeatureExtractorSet(motion_feature_fn);
    }

    RDFRegressionPtr cs_class_fn(new RDFRegression());
    cs_class_fn->Initialize(20);

    std::string predictor_foldername;
    ros_tools::getExpParam(predictor_foldername, "predictor_foldername", n);
    std::vector<CSClassificationPtr> model_set;
    for (unsigned int i = 0; i < total_timesteps; i++) {
      std::string predictor_filename = predictor_foldername + std::to_string(i)+".pred";
      CSClassificationPtr model(cs_class_fn->Clone());
      model->Load(predictor_filename);
      model_set.push_back(model);
    }

    policy.reset(new LearntForwardTraining(feature_fn, model_set));
  } else if (option_policy == 6) {
    ConcatenateFeatureExtractorPtr feature_fn(new ConcatenateFeatureExtractor());
    {
      IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
      ig_feature_fn->Initialize(n);
      feature_fn->AddFeatureExtractorSet(ig_feature_fn);
      LastVertexPoseFeatureExtractorPtr pose_feature_fn(new LastVertexPoseFeatureExtractor(LastVertexPoseFeatureExtractor::ABS_POS_ONLY));
      feature_fn->AddFeatureExtractorSet(pose_feature_fn);
      MotionLengthFeatureExtractorPtr motion_feature_fn(new MotionLengthFeatureExtractor());
      feature_fn->AddFeatureExtractorSet(motion_feature_fn);
    }

    RDFRegressionPtr cs_class_fn(new RDFRegression());
    cs_class_fn->Initialize(20);

    std::string predictor_filename;
    ros_tools::getExpParam(predictor_filename, "predictor_filename", n);
    CSClassificationPtr model(cs_class_fn->Clone());
    if(!model->Load(predictor_filename)) {
      ROS_ERROR_STREAM("Couldnt load predictor "<<predictor_filename);
      return EXIT_FAILURE;
    }

    policy.reset(new LearntAggrevate(feature_fn, model));
  }

  // Initialize
  for (auto &it : test_dataset) {
    Action action = *it.second.action_set.begin();
    State new_state;
    state_belief_tr_fn->UpdateState(it.first, action, it.second, new_state);
    it.first = new_state;
  }

  // Test policy
  std::vector< std::vector<double> > objective_trajectory_set;
  for (auto &it : test_dataset) {
    std::vector<double> objective_trajectory;
    for (unsigned int time_step = 0; time_step < total_timesteps; time_step++) {
      std::set<Action> action_set = it.second.action_set;
      for (auto a : it.first.action_set)
        action_set.erase(a);
      Action action;
      policy->Compute(time_step, it.first, action_set, action);

      State new_state;
      state_belief_tr_fn->UpdateState(it.first, action, it.second, new_state);
      if (cost_fn->Cost(new_state, it.second) <= cost_budget) {
        it.first = new_state;
      }

      double val = obj_fn->Value(it.first, it.second);
      objective_trajectory.push_back(val);

      ROS_ERROR_STREAM("Timestep: "<<time_step<<" Value: "<<val<<" Cost: "<<cost_fn->Cost(it.first, it.second));
    }
    objective_trajectory_set.push_back(objective_trajectory);
  }

  // Write output
  std::string results_filename;
  ros_tools::getExpParam(results_filename,"results_filename", n);
  std::ofstream  file(results_filename, std::ofstream::out);

  if (!file.good()) {
    ROS_ERROR_STREAM("cant open plan path");
    return EXIT_FAILURE;
  }

  for (auto &it1 : objective_trajectory_set) {
    for (auto &it2: it1) {
      file << it2 << " ";
    }
    file << std::endl;
  }
  file.close();


  unsigned int disp_idx = 0;
  tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(tf::Transform::getIdentity(), ros::Time::now(), "world", "sensor"));
  ros::Duration(2.0).sleep();
  su::DisplayStateActionObservationSequence(test_dataset[disp_idx].first, pub_pcl, br, 1.0);

}
