/* Copyright 2015 Sanjiban Choudhury
 * example_forward_training.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */


#include <ros/ros.h>
#include "ig_learning/state.h"
#include "ig_learning/state_utils.h"
#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_learning_experiments/io_utils.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"

#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"
#include "ig_learning/cost_functions/constant_cost_function.h"
#include "ig_learning/cost_functions/motion_cost_function.h"


#include "ig_learning/clairvoyant_oracles/one_step_reward_clairvoyant_oracle.h"
#include "ig_learning/clairvoyant_oracles/one_step_reward_motion_clairvoyant_oracle.h"
#include "ig_learning/clairvoyant_oracles/offline_solver_clairvoyant_oracle.h"
#include "ig_learning/offline_solvers/generalized_cost_benefit.h"
#include "ig_learning/feature_extractor/ig_feature_extractor.h"
#include "ig_learning/feature_extractor/last_vertex_pose_feature_extractor.h"
#include "ig_learning/feature_extractor/motion_length_feature_extractor.h"

#include "ig_learning/cs_classification/rdf_regression_cs_classification.h"
#include "ig_learning/imitation_learning/forward_training.h"

using namespace ig_learning;

namespace io = io_utils;
namespace su = state_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "test_state_transition");
  ros::NodeHandle n("~");
  std::srand(0);

  ros::Publisher pub_map = n.advertise<visualization_msgs::MarkerArray>("belief", 1);
  ros::Duration(1.0).sleep();


  //Step 1: Load input
  ForwardTraining::Input input;
  {
    double model_res;
    ros_tools::getExpParam(model_res,"model_res", n);
    std::vector<WorldMap> world_map_set;
    std::string training_foldername;
    ros_tools::getExpParam(training_foldername,"training_foldername", n);
    int num_train;
    ros_tools::getExpParam(num_train,"num_train", n);
    io::LoadWorldMapSet(training_foldername, num_train, model_res, world_map_set);
    int total_timesteps;
    ros_tools::getExpParam(total_timesteps,"total_timesteps", n);
    input.total_timesteps = total_timesteps;

    for (auto it : world_map_set) {
      State state;
      InitializeState(n, state);
      input.train_dataset.emplace_back(state, it);
    }
  }

  //Step 2: Create params
  ForwardTraining::Parameters params;
  {
    StateTransitionPtr state_belief_tr_fn;
    ObjectiveFunctionPtr obj_fn;
    CostFunctionPtr cost_fn;
    double cost_budget = 0;
    FeatureExtractorPtr feature_fn;
    ClairvoyantOraclePtr oracle_fn;
    CSClassificationPtr cs_class_fn;
    ForwardTraining::ActionSelection action_selection;


    int option_problem_setup;
    ros_tools::getExpParam(option_problem_setup,"option_problem_setup", n);
    switch (option_problem_setup) {
      case 1: {
        state_belief_tr_fn.reset(new PclLookupStateBeliefTransition());
        boost::static_pointer_cast<PclLookupStateBeliefTransition>(state_belief_tr_fn)->Initialize(n);
        obj_fn.reset( new PclVoxelMatchObjectiveFunction());
        cost_fn.reset( new ConstantCostFunction(0));
        cost_budget = std::numeric_limits<double>::infinity();
        break;
      }
      case 2: {
        state_belief_tr_fn.reset(new PclLookupStateBeliefTransition());
        boost::static_pointer_cast<PclLookupStateBeliefTransition>(state_belief_tr_fn)->Initialize(n);
        obj_fn.reset( new PclVoxelMatchObjectiveFunction());
        cost_fn.reset( new MotionCostFunction());
        ros_tools::getExpParam(cost_budget,"cost_budget", n);
        break;
      }
    }

    int option_feature;
    ros_tools::getExpParam(option_feature,"option_feature", n);
    switch (option_feature) {
      case 1: {
        feature_fn.reset(new ConcatenateFeatureExtractor());
        IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
        ig_feature_fn->Initialize(n);
        boost::static_pointer_cast<ConcatenateFeatureExtractor>(feature_fn)->AddFeatureExtractorSet(ig_feature_fn);
        LastVertexPoseFeatureExtractorPtr pose_feature_fn(new LastVertexPoseFeatureExtractor(LastVertexPoseFeatureExtractor::REL_POS_ONLY));
        boost::static_pointer_cast<ConcatenateFeatureExtractor>(feature_fn)->AddFeatureExtractorSet(pose_feature_fn);
        break;
      }
      case 2: {
        feature_fn.reset(new ConcatenateFeatureExtractor());
        IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
        ig_feature_fn->Initialize(n);
        boost::static_pointer_cast<ConcatenateFeatureExtractor>(feature_fn)->AddFeatureExtractorSet(ig_feature_fn);
        LastVertexPoseFeatureExtractorPtr pose_feature_fn(new LastVertexPoseFeatureExtractor(LastVertexPoseFeatureExtractor::ABS_POS_ONLY));
        boost::static_pointer_cast<ConcatenateFeatureExtractor>(feature_fn)->AddFeatureExtractorSet(pose_feature_fn);
        MotionLengthFeatureExtractorPtr motion_feature_fn(new MotionLengthFeatureExtractor());
        boost::static_pointer_cast<ConcatenateFeatureExtractor>(feature_fn)->AddFeatureExtractorSet(motion_feature_fn);
        break;
      }
      case 3: {
        feature_fn.reset(new ConcatenateFeatureExtractor());
        IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
        ig_feature_fn->Initialize(n);
        boost::static_pointer_cast<ConcatenateFeatureExtractor>(feature_fn)->AddFeatureExtractorSet(ig_feature_fn);
        break;
      }
    }

    int option_oracle;
    ros_tools::getExpParam(option_oracle,"option_oracle", n);
    switch (option_oracle) {
      case 1: {
        PclLookupStateTransitionPtr state_tr_fn(new PclLookupStateTransition());
        oracle_fn.reset(new OneStepRewardClairvoyantOracle(obj_fn, state_tr_fn));
        break;
      }
      case 2: {
        boost::shared_ptr<GeneralizedCostBenefit> solver(new GeneralizedCostBenefit());
        OfflineSolverParams solver_input;
        solver_input.state_transition.reset(new PclLookupStateTransition());
        solver_input.obj_fn = obj_fn;
        solver_input.cost_fn = cost_fn;
        solver_input.cost_budget = cost_budget;
        solver_input.budget = input.total_timesteps;
        oracle_fn.reset(new OfflineSolverClairvoyantOracle(solver, solver_input));
        break;
      }
      case 3: {
        PclLookupStateTransitionPtr state_tr_fn(new PclLookupStateTransition());
        oracle_fn.reset(new OneStepRewardMotionClairvoyantOracle(obj_fn, state_tr_fn));
        break;
      }
    }

    int option_learner;
    ros_tools::getExpParam(option_learner,"option_learner", n);
    switch (option_learner) {
      case 1: {
        cs_class_fn.reset(new RDFRegression());
        boost::static_pointer_cast<RDFRegression>(cs_class_fn)->Initialize(20);
        break;
      }
    }

    std::string option_action_selection;
    ros_tools::getExpParam(option_action_selection, "option_action_selection", n);
    if (option_action_selection.compare("ALL") == 0) {
      action_selection = ForwardTraining::ActionSelection(ForwardTraining::ActionSelection::ALL);
    } else {
      int num_action_tries;
      ros_tools::getExpParam(num_action_tries,"num_action_tries", n);
      action_selection = ForwardTraining::ActionSelection(ForwardTraining::ActionSelection::SUBSET,
                                                          num_action_tries);
    }

    //B. State only
    params.state_belief_transition = state_belief_tr_fn;
    params.objective_fn = obj_fn;
    params.cost_fn = cost_fn;
    params.cost_budget = cost_budget;
    params.oracle_fn = oracle_fn;
    params.feature_fn = feature_fn;
    params.cs_class_fn = cs_class_fn;
    params.action_selection = action_selection;
  }

  //Step 3: Initialize state
  for (auto &it : input.train_dataset) {
    Action action = *it.second.action_set.begin();
    State new_state;
    params.state_belief_transition->UpdateState(it.first, action, it.second, new_state);
    it.first = new_state;
  }

//  //Step 4: Lets just see ze belief
//  for (auto it : input.train_dataset) {
//    pub_map.publish(VisualizeBelief(it.first));
//    ros::Duration(1.0).sleep();
//  }

  //Step 4: Do forward training
  ForwardTraining::Output output;
  ForwardTraining forward_train(params);
  forward_train.Train(input, output);

  //Step 5: Save predictor
  {
    std::string predictor_foldername;
    ros_tools::getExpParam(predictor_foldername, "predictor_foldername", n);
    for (unsigned int i = 0; i < output.model_set.size(); i++) {
      std::string predictor_filename = predictor_foldername + std::to_string(i)+".pred";
      output.model_set[i]->Save(predictor_filename);
    }
  }
}
