/* Copyright 2015 Sanjiban Choudhury
 * scratch.cpp
 *
 *  Created on: Aug 4, 2016
 *      Author: Sanjiban Choudhury
 */


#include "ig_learning_experiments/io_utils.h"

int main(int argc, char **argv) {
  std::vector< std::vector<unsigned int> > expert_action_sequence;
  ig_learning::io_utils::LoadPrecomputedExpert("/home/aeroscout/data/information_planning/synthetic_datasets_gazebo/office_desk_dataset/precomputed_expert/gcb.txt", expert_action_sequence);
  std::cout << "Num trajs " << expert_action_sequence.size() << " Length of first one "<<expert_action_sequence.front().size()<<"\n";
  for (auto it : expert_action_sequence) {
    for (auto it2 : it) {
      std::cout << it2 <<" ";
    }
    std::cout << "\n";
  }
}


