/* Copyright 2015 Sanjiban Choudhury
 * spin_around_camera.cpp
 *
 *  Created on: Jul 26, 2016
 *      Author: Sanjiban Choudhury
 */

#include <stdint.h>
#include <ros/ros.h>
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include "flying_gazebo_stereo_cam/controller.hpp"

namespace iar = ig_active_reconstruction;
namespace fgsc = flying_gazebo_stereo_cam;

int main(int argc, char **argv) {
  ros::init(argc, argv, "create_pcd_database");
  ros::NodeHandle n("~");

  // Step 1: Initialize controller
  std::string model_name;
  ros_tools::getExpParam(model_name,"model_name");
  fgsc::Controller controller(model_name);

  // Step 4: Go to view space and take measurement
  unsigned int id = 0;

  for (double yaw = 0; yaw <= 30; yaw += 0.1) {
    movements::Pose pose;
    pose.position = Eigen::Vector3d::Zero();
    pose.position.z() = 1;

    Eigen::AngleAxisd rollAngle(yaw, Eigen::Vector3d::UnitX());
    Eigen::AngleAxisd pitchAngle(M_PI*0.5, Eigen::Vector3d::UnitY());
    Eigen::AngleAxisd yawAngle(0.0, Eigen::Vector3d::UnitZ());

    pose.orientation = yawAngle * pitchAngle * rollAngle;
    controller.moveTo(pose);
    ros::Duration(0.1).sleep();
  }
}



