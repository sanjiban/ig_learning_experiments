/* Copyright 2015 Sanjiban Choudhury
 * test_state_transition.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning/state.h"
#include "ig_learning/state_utils.h"
#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_learning_experiments/io_utils.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_learning/objective_functions/occupied_cells_objective_function.h"
#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"
#include "ig_learning/cost_functions/motion_cost_function.h"
#include "ig_learning/offline_solvers/lazy_greedy_nemhauser.h"
#include "ig_learning/offline_solvers/generalized_cost_benefit.h"
#include "ig_learning/clairvoyant_oracles/one_step_reward_clairvoyant_oracle.h"

#include "ig_learning/feature_extractor/ig_feature_extractor.h"
#include "ig_learning/feature_extractor/last_vertex_pose_feature_extractor.h"

namespace igl = ig_learning;
namespace io = igl::io_utils;
namespace su = igl::state_utils;

#define STATEFN 2
#define OBJFN 2
#define VIZ 1
#define TEST 1
#define PRESIM 0

int main(int argc, char **argv) {
  ros::init(argc, argv, "test_state_transition");
  ros::NodeHandle n("~");
  std::srand(0);

  // Create publishers
  ros::Publisher pub_map = n.advertise<visualization_msgs::MarkerArray>("belief", 1);
  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("output_pcl", 1);
  ros::Publisher pub_mod_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("model_pcl", 1);

  ros::Duration(1.0).sleep();

  // Load State Transition
  igl::WorldMap world_map;
  su::LoadWorldMap(n, world_map);
#if STATEFN == 1
  igl::PclLookupStateBeliefTransition pcl_lookup_fn;
  pcl_lookup_fn.Initialize(n);
  igl::StateTransitionPtr state_transition(new igl::PclLookupStateBeliefTransition(pcl_lookup_fn));
#elif STATEFN == 2
  igl::PclLookupStateTransition pcl_lookup_fn;
  igl::StateTransitionPtr state_transition(new igl::PclLookupStateTransition(pcl_lookup_fn));
#else
#endif

  // Objective fn
#if OBJFN == 1
  igl::ObjectiveFunctionPtr obj_fn(new igl::OccupiedCellsObjectiveFunction(Eigen::Vector3d(-0.3, -0.3, 0),
                                                                           Eigen::Vector3d(0.3, 0.3, 0.6)));
#elif OBJFN == 2
  igl::ObjectiveFunctionPtr obj_fn( new igl::PclVoxelMatchObjectiveFunction());
  world_map.model_pcl.header.frame_id = "world";
  world_map.model_pcl.header.stamp = ros::Time::now().toSec();
  pub_mod_pcl.publish(world_map.model_pcl);
#else
#endif

  // Create initial state
  igl::State state;
  ROS_ERROR_STREAM("initial");
  igl::InitializeState(n, state);
  state_transition->UpdateState(state, *world_map.action_set.begin(), world_map, state);
  world_map.action_set.erase(*world_map.action_set.begin());

  // Do some sim
  /*
  igl::State state_bck(state);
  unsigned int count = 2; //action_set.size();
  for (unsigned int i = 0; i < count; i++) {
    ROS_ERROR_STREAM(i);
    std::set<igl::Action>::iterator rand_action = action_set.begin();
    std::advance(rand_action , std::rand() % action_set.size());
    state_transition->UpdateState(state, *rand_action, state);
    action_set.erase(rand_action);
    ROS_ERROR_STREAM("Objective "<<obj_fn.Value(state)<<" Old: "<<obj_fn.Value(state_bck));
  }
    ros::Duration(1.0).sleep();
   */

#if TEST == 1
#if PRESIM == 1
  for (int i = 0; i < 3; i++) {
    state_transition->UpdateState(state, *world_map.action_set.begin(), world_map, state);
    world_map.action_set.erase(*world_map.action_set.begin());
  }
#endif

  igl::OfflineSolverParams input;
  input.obj_fn = obj_fn;
  input.state_transition = state_transition;
  input.world_map = world_map;
  input.state0 = state;
  input.budget = 10;
  input.cost_fn.reset(new igl::MotionCostFunction());
  input.cost_budget = 25;

  igl::OfflineSolverOutput output;

  //igl::LazyGreedyNemhauser solver;
  igl::GeneralizedCostBenefit solver;
  solver.Solve(input, output);

  igl::State new_state = output.state_final;
  std::cerr << "Obj: ";
  for (auto it : output.objective_trajectory)
    std::cerr << it << " ";
  std::cerr << std::endl;
  std::cerr << "Cost: ";
  for (auto it : output.cost_trajectory)
    std::cerr << it << " ";
  std::cerr << std::endl;
#elif TEST == 2
  igl::IGFeatureExtractor feat_fn;
  feat_fn.Initialize(n);

  igl::Action action = *std::next(world_map.action_set.begin(), 0);
  igl::State new_state;
  state_transition->UpdateState(state, action, world_map, new_state);


  for (auto act : world_map.action_set) {
    std::vector<double> feat_vec;
    feat_fn.GetFeature(state, act, feat_vec);
    std::cerr << "Old ";
    for (auto it : feat_vec)
      std::cerr << it <<" ";
    std::cerr << std::endl;

    feat_fn.GetFeature(new_state, act, feat_vec);
    std::cerr << "New ";
    for (auto it : feat_vec)
      std::cerr << it <<" ";
    std::cerr << std::endl;
  }

  /*
  igl::LastVertexPoseFeatureExtractor feat_fn2(LastVertexPoseFeatureExtractor::ABS_POS_ONLY);
  std::vector<double> feat_vec2;
  feat_fn2.GetFeature(new_state, action, feat_vec2);

  for (auto it : feat_vec2)
    ROS_ERROR_STREAM("Val Fn2 : "<<it);
   */
#elif TEST == 3
  {
    igl::State new_state(state);
  }
  ROS_ERROR_STREAM("hereA");
  //B. State only
  igl::PclLookupStateTransitionPtr state_tr_fn(new igl::PclLookupStateTransition());
  //C. Objective fn
  igl::PclVoxelMatchObjectiveFunctionPtr obj_fn( new igl::PclVoxelMatchObjectiveFunction());
  //D. Oracle fn
  igl::OneStepRewardClairvoyantOraclePtr oracle_fn(new igl::OneStepRewardClairvoyantOracle(obj_fn, state_tr_fn));
  for (int i = 0; i < 100; i++)
  {
    ROS_ERROR_STREAM(i);
    //    igl::State state_bck;
    //    state_transition->UpdateState(state,
    //                                  igl::state_utils::GetRandomAction(world_map.action_set)
    //                                  , world_map, state_bck);
    double qval = oracle_fn->Value(state, igl::state_utils::GetRandomAction(world_map.action_set), world_map);
    ROS_ERROR_STREAM(qval);
  }
  ROS_ERROR_STREAM("hereB");

  ros::Duration(100.0).sleep();
#elif TEST ==4
  ROS_ERROR_STREAM("woof");
  igl::State state_bck(state);
  ROS_ERROR_STREAM("meow");
  unsigned int count = 1; //action_set.size();
  for (unsigned int i = 0; i < count; i++) {
    ROS_ERROR_STREAM(i);
    std::set<igl::Action>::iterator rand_action = world_map.action_set.begin();
    std::advance(rand_action , std::rand() % world_map.action_set.size());
    igl::State new_state(state);
    state_transition->UpdateState(state, *rand_action, world_map, new_state);
    /*  ROS_ERROR_STREAM("A");
    state = new_state;
    ROS_ERROR_STREAM("B");
    double val1 = obj_fn->Value(state_bck, world_map);
    ROS_ERROR_STREAM("C");
    double val2 = obj_fn->Value(state, world_map);
    ROS_ERROR_STREAM("D");*/
    igl::State new_state2(new_state);
    rand_action = world_map.action_set.begin();
    std::advance(rand_action , std::rand() % world_map.action_set.size());

    state_transition->UpdateState(new_state, *rand_action, world_map, new_state2);
    ROS_ERROR_STREAM("E");
    //ROS_ERROR_STREAM("Objective "<<val2<<" Old: "<<val1);
  }
  ros::Duration(1.0).sleep();
#else
#endif


#if VIZ == 1
  tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(tf::Transform::getIdentity(), ros::Time::now(), "world", "sensor"));
  ros::Duration(2.0).sleep();
  su::DisplayStateActionObservationSequence(new_state, pub_pcl, br, 1.0);
#elif VIZ ==2
  pub_map.publish(igl::VisualizeBelief(new_state));
#else
#endif
  ros::Duration(1.0).sleep();
}



