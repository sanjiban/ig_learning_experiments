/* Copyright 2015 Sanjiban Choudhury
 * post_process_sampled_pcd.cpp
 *
 *  Created on: Jul 11, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning_experiments/io_utils.h"
#include <pcl/filters/crop_box.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>

using namespace ig_learning;

namespace io = io_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "post_process_sample_pcd");
  ros::NodeHandle n("~");

  std::string pcd_folder_path;
  if (!n.getParam("pcd_folder", pcd_folder_path)) {
    ROS_ERROR_STREAM("Couldnt load pcd");
    return EXIT_FAILURE;
  }

  int num_pcd;
  if (!n.getParam("num_pcd", num_pcd)) {
    ROS_ERROR_STREAM("Couldnt load num pcd");
    return EXIT_FAILURE;
  }

  std::vector<float> bbox;
  if (!n.getParam("bbox", bbox)) {
    ROS_ERROR_STREAM("Couldnt load bbox");
    return EXIT_FAILURE;
  }

  for (unsigned i = 0; i < num_pcd; i++) {
     std::stringstream ss;
     ss << pcd_folder_path << i << ".pcd";
     pcl::PointCloud<pcl::PointXYZ>::Ptr original_pcl(new pcl::PointCloud<pcl::PointXYZ>());
     if (pcl::io::loadPCDFile<pcl::PointXYZ> (ss.str(), *original_pcl) == -1) {
       return EXIT_FAILURE;
     }


     pcl::CropBox<pcl::PointXYZ> crop;
     Eigen::Vector4f lb(bbox[0], bbox[2], bbox[4], 0);
     Eigen::Vector4f ub(bbox[1], bbox[3], bbox[5], 0);
     crop.setMin(lb);
     crop.setMax(ub);
     crop.setInputCloud(original_pcl);
     pcl::PointCloud<pcl::PointXYZ>::Ptr cropped_pcl(new pcl::PointCloud<pcl::PointXYZ>());
     crop.filter(*cropped_pcl);


     pcl::VoxelGrid<pcl::PointXYZ> vox;
     vox.setLeafSize (0.01f, 0.01f, 0.01f);
     vox.setInputCloud (cropped_pcl);
     pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_pcl(new pcl::PointCloud<pcl::PointXYZ>());
     vox.filter(*filtered_pcl);

//     pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_pcl(new pcl::PointCloud<pcl::PointXYZ>());
//     filtered_pcl = cropped_pcl;


     if (filtered_pcl->empty()) {
       filtered_pcl->width    = 1;
       filtered_pcl->height   = 1;
       filtered_pcl->is_dense = false;
       filtered_pcl->points.resize (filtered_pcl->width * filtered_pcl->height);
       const float bad_point = std::numeric_limits<float>::quiet_NaN();
       filtered_pcl->points[0] = pcl::PointXYZ(bad_point, bad_point, bad_point);
     }

     if (filtered_pcl->size() > 0)
       pcl::io::savePCDFile(ss.str(), *filtered_pcl, true);
   }
}



