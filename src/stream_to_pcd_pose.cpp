/* Copyright 2015 Sanjiban Choudhury
 * stream_to_pcd_pose.cpp
 *
 *  Created on: Jul 18, 2016
 *      Author: Sanjiban Choudhury
 */

#include <stdint.h>
#include <ros/ros.h>
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include "flying_gazebo_stereo_cam/controller.hpp"
#include <pcl/common/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_listener.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/voxel_grid.h>

namespace iar = ig_active_reconstruction;
namespace fgsc = flying_gazebo_stereo_cam;

pcl::PointCloud<pcl::PointXYZ> cloud;
bool got_cloud = false;
ros::Time msg_time;

void PclCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &msg) {
  cloud = *msg;
  got_cloud = true;
  pcl_conversions::fromPCL(msg->header.stamp, msg_time);
}


int main(int argc, char **argv) {
  ros::init(argc, argv, "stream_to_pcd_pose");
  ros::NodeHandle n("~");

  tf::TransformListener listener;
  // Step 1: Create pcl subscriber
  std::string sensor_topic;
  ros_tools::getExpParam(sensor_topic,"sensor_topic");
  ros::Subscriber sub_pcl = n.subscribe(sensor_topic, 1, &PclCallback ); // we want only latest message
  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("/transformed_pcl", 100);
  ros::Duration(1.0).sleep();

  std::string pcd_folder;
  ros_tools::getExpParam(pcd_folder,"pcd_folder");

  unsigned int counter = 0;
  ros::Rate loop_rate(50.0);
  while (ros::ok()) {
    ros::spinOnce();
    if (got_cloud) {
     got_cloud = false;

     tf::StampedTransform sensor_to_world_tf;
     bool got_tf = false;
     while (!got_tf) {
       try {
         listener.lookupTransform("world", cloud.header.frame_id, msg_time, sensor_to_world_tf);
         got_tf = true;
       } catch(tf::TransformException& ex) {
         //ROS_ERROR_STREAM("Couldnt find transform "<<ex.what());
         got_tf = false;
         ros::Duration(0.05).sleep();
         continue;
       }
     }


     ROS_ERROR_STREAM("Trans: " <<
                      sensor_to_world_tf.getOrigin().x() <<
                      sensor_to_world_tf.getOrigin().y() <<
                      sensor_to_world_tf.getOrigin().z() <<
                      "Quat: " <<
                      sensor_to_world_tf.getRotation().x() <<
                      sensor_to_world_tf.getRotation().y() <<
                      sensor_to_world_tf.getRotation().z() <<
                      sensor_to_world_tf.getRotation().w());

     pcl::PointCloud<pcl::PointXYZ>::Ptr original_pcl (new pcl::PointCloud<pcl::PointXYZ>);
     *original_pcl = cloud;

     pcl::PointCloud<pcl::PointXYZ>::Ptr cropped_pcl (new pcl::PointCloud<pcl::PointXYZ>);

     pcl::CropBox<pcl::PointXYZ> crop;
     Eigen::Vector4f lb(-1e6, -1e6, 0, 1);
     Eigen::Vector4f ub(1e6, 1e6, 4, 1);
     crop.setMin(lb);
     crop.setMax(ub);
     crop.setInputCloud(original_pcl);
     crop.filter(*cropped_pcl);

     /*
     pcl::VoxelGrid<pcl::PointXYZ> vox;
     vox.setLeafSize (0.01f, 0.01f, 0.01f);
     vox.setInputCloud (cropped_pcl);
     vox.filter(*cropped_pcl);
*/


     pcl::PointCloud<pcl::PointXYZ>::Ptr final_cloud (new pcl::PointCloud<pcl::PointXYZ>);
     final_cloud = cropped_pcl;
     /*
     pcl_ros::transformPointCloud(*cropped_pcl, *transformed_cloud, sensor_to_world_tf);
     transformed_cloud->header.frame_id = "world";
     transformed_cloud->header.stamp = ros::Time::now().toSec();
     pub_pcl.publish(transformed_cloud);
     */
     pcl::io::savePCDFile(pcd_folder + std::to_string(counter) + ".pcd", *final_cloud, true);

     // Write output
     std::string results_filename = pcd_folder + std::to_string(counter) + ".pose";
     std::ofstream  file(results_filename, std::ofstream::out);
     file << sensor_to_world_tf.getOrigin().x() << " " <<
         sensor_to_world_tf.getOrigin().y() << " " <<
         sensor_to_world_tf.getOrigin().z() << " " <<
         sensor_to_world_tf.getRotation().x() << " " <<
         sensor_to_world_tf.getRotation().y() << " " <<
         sensor_to_world_tf.getRotation().z() << " " <<
         sensor_to_world_tf.getRotation().w();
     file.close();
     counter ++;
    }
    loop_rate.sleep();
  }
}
