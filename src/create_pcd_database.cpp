/* Copyright 2015 Sanjiban Choudhury
 * create_pcd_database.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include <stdint.h>
#include <ros/ros.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/voxel_grid.h>
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include "flying_gazebo_stereo_cam/controller.hpp"
#include <pcl/common/transforms.h>
#include <pcl_ros/point_cloud.h>

namespace iar = ig_active_reconstruction;
namespace fgsc = flying_gazebo_stereo_cam;

pcl::PointCloud<pcl::PointXYZ> cloud;
bool got_cloud = false;

void PclCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &msg) {
  cloud = *msg;
  got_cloud = true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "create_pcd_database");
  ros::NodeHandle n("~");

  // Step 1: Load the viewspace
  std::string viewspace_file_path;
  ros_tools::getExpParam(viewspace_file_path,"viewspace_file_path", n);
  iar::views::ViewSpace view_space;
  view_space.loadFromFile(viewspace_file_path);

  // Step 2: Initialize controller
  std::string model_name;
  ros_tools::getExpParam(model_name,"model_name");
  fgsc::Controller controller(model_name);

  // Step 3: Create pcl subscriber
  std::string sensor_topic;
  ros_tools::getExpParam(sensor_topic,"sensor_topic");
  ros::Subscriber sub_pcl = n.subscribe(sensor_topic, 1, &PclCallback ); // we want only latest message
  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("/transformed_pcl", 100);
  ros::Duration(10.0).sleep();

  // Step 4: Load pcd folder where to save
  std::string pcd_folder;
  ros_tools::getExpParam(pcd_folder,"pcd_folder");

  // Step 4: Go to view space and take measurement
  unsigned int id = 0;
  for (iar::views::ViewSpace::Iterator it = view_space.begin(); it != view_space.end(); ++it, id++) {
    ROS_ERROR_STREAM("Processing view id: "<<id<< "pcd: "<<pcd_folder);
    controller.moveTo(it->pose());
    ros::Duration(0.5).sleep(); //new pose should be in effect
    got_cloud = false;
    while (!got_cloud) {
      ros::spinOnce();
      ros::Duration(0.1).sleep();
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr original_pcl(new pcl::PointCloud<pcl::PointXYZ>());
    *original_pcl = cloud;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cropped_pcl(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::CropBox<pcl::PointXYZ> crop;
    Eigen::Vector4f lb(-1e6, -1e6, 0, 0);
    Eigen::Vector4f ub(1e6, 1e6, 10, 0);
    crop.setMin(lb);
    crop.setMax(ub);
    crop.setInputCloud(original_pcl);
    crop.filter(*cropped_pcl);

    if (cropped_pcl->empty()) {
      cropped_pcl->width    = 1;
      cropped_pcl->height   = 1;
      cropped_pcl->is_dense = false;
      cropped_pcl->points.resize (cropped_pcl->width * cropped_pcl->height);
      const float bad_point = std::numeric_limits<float>::quiet_NaN();
      cropped_pcl->points[0] = pcl::PointXYZ(bad_point, bad_point, bad_point);
    }

    /*
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f sensor_to_world;
    pcl::transformPointCloud(*cropped_pcl, *transformed_cloud, it->pose().position, it->pose().orientation);
    transformed_cloud->header.frame_id = "world";
    transformed_cloud->header.stamp = ros::Time::now().toSec();
    pub_pcl.publish(transformed_cloud);
    */

    pcl::VoxelGrid<pcl::PointXYZ> vox;
    vox.setLeafSize (0.01f, 0.01f, 0.01f); // 0.01f for bunny
    vox.setInputCloud (cropped_pcl);
    pcl::PointCloud<pcl::PointXYZ>::Ptr vox_pcl(new pcl::PointCloud<pcl::PointXYZ>());
    vox.filter(*vox_pcl);
    ROS_ERROR_STREAM("published voxed pcl");

    if (vox_pcl->empty()) {
      vox_pcl->width    = 1;
      vox_pcl->height   = 1;
      vox_pcl->is_dense = false;
      vox_pcl->points.resize (vox_pcl->width * vox_pcl->height);
      const float bad_point = std::numeric_limits<float>::quiet_NaN();
      vox_pcl->points[0] = pcl::PointXYZ(bad_point, bad_point, bad_point);
    }


    pcl::io::savePCDFile(pcd_folder + std::to_string(id) + ".pcd", *vox_pcl, true);
  }
}



