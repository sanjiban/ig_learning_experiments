/* Copyright 2015 Sanjiban Choudhury
 * play_dataset.cpp
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning/state.h"
#include "ig_learning/state_utils.h"
#include "ig_learning_experiments/io_utils.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_learning/state_transition/pcl_lookup_state_transition.h"

namespace igl = ig_learning;
namespace io = igl::io_utils;
namespace su = igl::state_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "test_state_transition");
  ros::NodeHandle n("~");
  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("pcl", 1);
  ros::Publisher pub_map = n.advertise<visualization_msgs::MarkerArray>("belief", 1);

  ros::Duration(1.0).sleep();

  // Load State Transition
  igl::WorldMap world_map;
  su::LoadWorldMap(n, world_map);

  igl::PclLookupStateBeliefTransition pcl_lookup_fn;
  pcl_lookup_fn.Initialize(n);
  igl::StateTransitionPtr state_transition(new igl::PclLookupStateBeliefTransition(pcl_lookup_fn));

  igl::State state;
  ROS_ERROR_STREAM("initial");
  igl::InitializeState(n, state);
  for (auto it : world_map.action_set) {
    state_transition->UpdateState(state, it, world_map, state);
    pub_map.publish(igl::VisualizeBelief(state));

    pcl::PointCloud<pcl::PointXYZ> cloud = state.observation_set.back();
    cloud.header.frame_id = "world";
    cloud.header.stamp = ros::Time::now().toSec();
    pub_pcl.publish(cloud);

    ROS_ERROR_STREAM("updated");
    ros::Duration(1.0).sleep();
  }

  /*
  tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(tf::Transform::getIdentity(), ros::Time::now(), "world", "sensor"));
  ros::Duration(2.0).sleep();
  su::DisplayStateActionObservationSequence(state, pub_pcl, br, 1.0);
  */
}

