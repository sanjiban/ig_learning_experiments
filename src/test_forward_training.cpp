/* Copyright 2015 Sanjiban Choudhury
 * test_forward_training.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning/state.h"
#include "ig_learning/state_utils.h"
#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_learning_experiments/io_utils.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"
#include "ig_learning/clairvoyant_oracles/one_step_reward_clairvoyant_oracle.h"
#include "ig_learning/feature_extractor/ig_feature_extractor.h"
#include "ig_learning/feature_extractor/last_vertex_pose_feature_extractor.h"
#include "ig_learning/cs_classification/rdf_regression_cs_classification.h"
#include "ig_learning/imitation_learning/forward_training.h"

using namespace ig_learning;

namespace io = io_utils;
namespace su = state_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "test_state_transition");
  ros::NodeHandle n("~");
  std::srand(0);

  //Step 1: Load input
  ForwardTraining::Input input;
  {
    double model_res;
    ros_tools::getExpParam(model_res,"model_res", n);
    std::vector<WorldMap> world_map_set;
    std::string test_foldername;
    ros_tools::getExpParam(test_foldername,"test_foldername", n);
    int num_test;
    ros_tools::getExpParam(num_test,"num_test", n);
    io::LoadWorldMapSet(test_foldername, num_test, model_res, world_map_set);
    int total_timesteps;
    ros_tools::getExpParam(total_timesteps,"total_timesteps", n);
    input.total_timesteps = total_timesteps;

    for (auto it : world_map_set) {
      State state;
      InitializeState(n, state);
      input.train_dataset.emplace_back(state, it);
    }
  }

  //Step 2: Create params
  ForwardTraining::Parameters params;
  {
    //A. State belief
    PclLookupStateBeliefTransitionPtr state_belief_tr_fn(new PclLookupStateBeliefTransition());
    state_belief_tr_fn->Initialize(n);
    //B. State only
    PclLookupStateTransitionPtr state_tr_fn(new PclLookupStateTransition());
    //C. Objective fn
    PclVoxelMatchObjectiveFunctionPtr obj_fn( new PclVoxelMatchObjectiveFunction());
    //D. Oracle fn
    OneStepRewardClairvoyantOraclePtr oracle_fn(new OneStepRewardClairvoyantOracle(obj_fn, state_tr_fn));
    //E. Feature fn
    ConcatenateFeatureExtractorPtr feature_fn(new ConcatenateFeatureExtractor());
    {
      IGFeatureExtractorPtr ig_feature_fn(new IGFeatureExtractor());
      ig_feature_fn->Initialize(n);
      feature_fn->AddFeatureExtractorSet(ig_feature_fn);
      LastVertexPoseFeatureExtractorPtr pose_feature_fn(new LastVertexPoseFeatureExtractor(LastVertexPoseFeatureExtractor::ABS_POS_ONLY));
      feature_fn->AddFeatureExtractorSet(pose_feature_fn);
    }
    //F. Cs Class fn
    RDFRegressionPtr cs_class_fn(new RDFRegression());
    cs_class_fn->Initialize(20);
    ForwardTraining::ActionSelection action_selection(ForwardTraining::ActionSelection::ALL);

    params.state_belief_transition = state_belief_tr_fn;
    params.objective_fn = obj_fn;
    params.oracle_fn = oracle_fn;
    params.feature_fn = feature_fn;
    params.cs_class_fn = cs_class_fn;
    params.action_selection = action_selection;
  }

  //Step 3: Initialize state
  for (auto &it : input.train_dataset) {
    Action action = *it.second.action_set.begin();
    State new_state;
    params.state_belief_transition->UpdateState(it.first, action, it.second, new_state);
    it.first = new_state;
  }

  //Step 4: Load model
  {
    std::string predictor_foldername;
    ros_tools::getExpParam(predictor_foldername, "predictor_foldername", n);

    std::vector <double> tmp;
    params.feature_fn->GetFeature(input.train_dataset.front().first,
                                   *input.train_dataset.front().second.action_set.begin(),
                                    tmp);
    unsigned int fdim = tmp.size();

    for (unsigned int i = 0; i < input.total_timesteps; i++) {
      std::string predictor_filename = predictor_foldername + std::to_string(i)+".pred";
      CSClassificationPtr model(params.cs_class_fn->Clone());
      model->Load(predictor_filename);

      for (auto &it : input.train_dataset) {
        std::set<Action> actions_selected = it.second.action_set; // TODO
        for (auto a : it.first.action_set)
          actions_selected.erase(a);

        Eigen::MatrixXd feature_point(actions_selected.size(), fdim);
        Eigen::VectorXd qval_point(actions_selected.size());
        unsigned int id = 0;
        for (auto a : actions_selected) {
          std::vector<double> feature_vec;
          params.feature_fn->GetFeature(it.first, a, feature_vec);
          double qval = params.oracle_fn->Value(it.first, a, it.second, input.total_timesteps - (i+1));
          for (unsigned int fidx = 0; fidx < feature_vec.size(); fidx++)
            feature_point(id, fidx) = feature_vec[fidx];
          qval_point[id] = qval;
          id++;
        }
        unsigned selected_index = model->Predict(feature_point);

        ROS_ERROR_STREAM("Selected action: "<<selected_index<<
                         " Qval: "<<qval_point[selected_index]<<
                         " Best: "<<qval_point.maxCoeff()<<
                         "Worst: "<<qval_point.minCoeff());

        Action selected_action = *std::next(actions_selected.begin(), selected_index);
        State new_state;
        params.state_belief_transition->UpdateState(it.first, selected_action, it.second, new_state);
        it.first = new_state;
      }

    }
  }

}



