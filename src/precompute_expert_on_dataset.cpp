/* Copyright 2015 Sanjiban Choudhury
 * precompute_expert_on_dataset.cpp
 *
 *  Created on: Aug 3, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning/state.h"
#include "ig_learning/state_utils.h"
#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_learning_experiments/io_utils.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"
#include "ig_learning/cost_functions/constant_cost_function.h"
#include "ig_learning/cost_functions/motion_cost_function.h"
#include "ig_learning/offline_solvers/generalized_cost_benefit.h"
#include "ig_learning/offline_solvers/lazy_greedy_nemhauser.h"

using namespace ig_learning;

namespace io = io_utils;
namespace su = state_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "precompute_expert_on_dataset");
  ros::NodeHandle n("~");

  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("output_pcl", 1);
  ros::Duration(1.0).sleep();

  // Load Stuff
  std::vector< std::pair<State, WorldMap> > train_dataset;
  int total_timesteps = 0;
  {
    double model_res;
    ros_tools::getExpParam(model_res,"model_res", n);
    std::vector<WorldMap> world_map_set;
    std::string train_foldername;
    ros_tools::getExpParam(train_foldername,"training_foldername", n);
    int num_train;
    ros_tools::getExpParam(num_train,"num_train", n);
    io::LoadWorldMapSet(train_foldername, num_train, model_res, world_map_set);
    ros_tools::getExpParam(total_timesteps,"total_timesteps", n);

    for (auto it : world_map_set) {
      State state;
      InitializeState(n, state);
      train_dataset.emplace_back(state, it);
    }
  }

  //Load problem setup
  StateTransitionPtr state_belief_tr_fn;
  ObjectiveFunctionPtr obj_fn;
  CostFunctionPtr cost_fn;
  double cost_budget = 0;

  int option_problem_setup;
  ros_tools::getExpParam(option_problem_setup,"option_problem_setup", n);
  switch (option_problem_setup) {
    case 1: {
      state_belief_tr_fn.reset(new PclLookupStateBeliefTransition());
      boost::static_pointer_cast<PclLookupStateBeliefTransition>(state_belief_tr_fn)->Initialize(n);
      obj_fn.reset( new PclVoxelMatchObjectiveFunction());
      cost_fn.reset( new ConstantCostFunction(0));
      cost_budget = std::numeric_limits<double>::infinity();
      break;
    }
    case 2: {
      state_belief_tr_fn.reset(new PclLookupStateBeliefTransition());
      boost::static_pointer_cast<PclLookupStateBeliefTransition>(state_belief_tr_fn)->Initialize(n);
      obj_fn.reset( new PclVoxelMatchObjectiveFunction());
      cost_fn.reset( new MotionCostFunction());
      ros_tools::getExpParam(cost_budget,"cost_budget", n);
      break;
    }
  }


  OfflineSolverParams solver_input;
  solver_input.state_transition.reset(new PclLookupStateTransition());
  solver_input.obj_fn = obj_fn;
  solver_input.cost_fn = cost_fn;
  solver_input.cost_budget = cost_budget;
  solver_input.budget = total_timesteps;

  OfflineSolverPtr solver;
  int option_solver;
  ros_tools::getExpParam(option_solver,"option_solver", n);

  if (option_solver == 1) {
    solver.reset(new GeneralizedCostBenefit());
  } else if (option_solver == 2) {
    solver.reset(new LazyGreedyNemhauser());
  }

  std::string precomputed_expert_filename;
  ros_tools::getExpParam(precomputed_expert_filename,"precomputed_expert_filename", n);
  std::ofstream  file(precomputed_expert_filename, std::ofstream::out);
  if (!file.good()) {
    ROS_ERROR_STREAM("cant open precomputed_expert_filename");
    return EXIT_FAILURE;
  }

  // Initialize
  for (auto &it : train_dataset) {
    Action action = *it.second.action_set.begin();
    State new_state;
    state_belief_tr_fn->UpdateState(it.first, action, it.second, new_state);
    it.first = new_state;
  }

  // Test policy
  for (auto &it : train_dataset) {
    solver_input.state0 = it.first;
    solver_input.world_map = it.second;

    OfflineSolverOutput solver_output;

    solver->Solve(solver_input, solver_output);

    for (auto it : solver_output.action_sequence)
      std::cerr<<it.id<<" ";
    std::cerr << std::endl;
    std::cerr << "Obj: "<<solver_output.objective_trajectory.back()
                  << "Cost: "<<solver_output.cost_trajectory.back() <<std::endl;

    for (auto it : solver_output.action_sequence)
        file << it.id << " ";
    file << std::endl;
    /*
    tf::TransformBroadcaster br;
    br.sendTransform(tf::StampedTransform(tf::Transform::getIdentity(), ros::Time::now(), "world", "sensor"));
    ros::Duration(2.0).sleep();
    su::DisplayStateActionObservationSequence(solver_output.state_final, pub_pcl, br, 1.0);
    */
  }
  file.close();
}


