/* Copyright 2015 Sanjiban Choudhury
 * play_kinect_data.cpp
 *
 *  Created on: Aug 2, 2016
 *      Author: Sanjiban Choudhury
 */


#include <ros/ros.h>
#include "ig_learning_experiments/io_utils.h"
#include <pcl/common/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <iostream>
using namespace ig_learning;

namespace io = io_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "play_kinect_data");
  ros::NodeHandle n("~");
  ros::Publisher pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("pcl", 1);

  ros::Duration(1.0).sleep();

  unsigned int num_pcd = 36;

  std::ifstream in("/home/aeroscout/Documents/meow2/trajectory_estimate.txt");
  std::vector<  std::pair<Eigen::Vector3d, Eigen::Quaterniond > > pose_set;
  for (unsigned int i = 0; i < num_pcd; i++) {
    double t;
    in >> t;
    Eigen::Vector3d position;
    Eigen::Quaterniond quat;
    in >> position.x() >> position.y() >> position.z();
    in >> quat.x() >> quat.y() >> quat.z() >> quat.w();
    pose_set.emplace_back(position, quat);
  }

  for (unsigned i = 0; i < num_pcd; i++) {
    ROS_ERROR_STREAM(i << " pos: " << pose_set[i].first.transpose());
    std::stringstream ss;
    ss << std::setw(4) << std::setfill('0') << i;
    std::string pcd_filename = "/home/aeroscout/Documents/meow2/data_" + ss.str() + ".pcd";
    pcl::PointCloud<pcl::PointXYZ>::Ptr original_pcl(new pcl::PointCloud<pcl::PointXYZ>());
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcd_filename, *original_pcl) == -1) {
      return EXIT_FAILURE;
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*original_pcl, *transformed_cloud, pose_set[i].first, pose_set[i].second);


    transformed_cloud->header.frame_id = "/world";
    transformed_cloud->header.stamp = ros::Time::now().toSec();
    pub_pcl.publish(transformed_cloud);
    ros::Duration(1.0).sleep();
  }
}


