/* Copyright 2015 Sanjiban Choudhury
 * post_process_model_pcd.cpp
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "ig_learning_experiments/io_utils.h"
#include <pcl/filters/crop_box.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include "ig_active_reconstruction_ros/param_loader.hpp"

using namespace ig_learning;

namespace io = io_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "post_process_model_pcd");
  ros::NodeHandle n("~");

  ros::Publisher pub_original_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("/original_pcl", 100);
  ros::Publisher pub_cropped_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("/cropped_pcl", 100);
  ros::Publisher pub_vox_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("/vox_pcl", 100);
  ros::Publisher pub_outlier_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >("/outlier_pcl", 100);

  ros::Duration(1.0).sleep();

  std::string pcd_folder_path;
  if (!n.getParam("pcd_folder", pcd_folder_path)) {
    ROS_ERROR_STREAM("Couldnt load pcd");
    return EXIT_FAILURE;
  }

  std::string viewspace_file_path;
  ros_tools::getExpParam(viewspace_file_path,"viewspace_file_path", n);

  std::string model_filename;
  if (!n.getParam("model_filename", model_filename)) {
    ROS_ERROR_STREAM("Couldnt load pcd");
    return EXIT_FAILURE;
  }


  std::vector<float> bbox;
  if (!n.getParam("bbox", bbox)) {
    ROS_ERROR_STREAM("Couldnt load bbox");
    return EXIT_FAILURE;
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr original_pcl(new pcl::PointCloud<pcl::PointXYZ>());

  // Load pcl
  io::LoadPCDSet(pcd_folder_path, viewspace_file_path, *original_pcl);

  original_pcl->header.frame_id = "world";
  original_pcl->header.stamp = ros::Time::now().toSec();
  //pub_original_pcl.publish(original_pcl);
  ros::Duration(0.1).sleep();
  ROS_ERROR_STREAM("published original pcl");

  pcl::CropBox<pcl::PointXYZ> crop;
  Eigen::Vector4f lb(bbox[0], bbox[2], bbox[4], 0);
  Eigen::Vector4f ub(bbox[1], bbox[3], bbox[5], 0);
  crop.setMin(lb);
  crop.setMax(ub);
  crop.setInputCloud(original_pcl);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cropped_pcl(new pcl::PointCloud<pcl::PointXYZ>());
  crop.filter(*cropped_pcl);

  cropped_pcl->header.frame_id = "world";
  cropped_pcl->header.stamp = ros::Time::now().toSec();
  pub_cropped_pcl.publish(cropped_pcl);
  ros::Duration(0.1).sleep();
  ROS_ERROR_STREAM("published cropped pcl");

  /*
  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> outlier;
  outlier.setMeanK(8);
  outlier.setStddevMulThresh (1.0);
  outlier.setInputCloud (cropped_pcl);
  pcl::PointCloud<pcl::PointXYZ>::Ptr outlier_pcl(new pcl::PointCloud<pcl::PointXYZ>());
  outlier.filter (*outlier_pcl);
*/


  /*  outlier_pcl->header.frame_id = "world";
  outlier_pcl->header.stamp = ros::Time::now().toSec();
  pub_outlier_pcl.publish(outlier_pcl);
  ros::Duration(0.1).sleep();
   */

/*
  pcl::ConditionalRemoval<pcl::PointXYZ> cond;
  pcl::ConditionOr<pcl::PointXYZ>::Ptr range_cond (new
                                                   pcl::ConditionOr<pcl::PointXYZ> ());
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                            pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT, 0.1)));
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                            pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, 0.15)));
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                            pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT, -0.2)));
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                            pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, -0.15)));
  cond.setCondition(range_cond);

  cond.setInputCloud(outlier_pcl);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cond_pcl(new pcl::PointCloud<pcl::PointXYZ>());
  cond.filter(*cond_pcl);

  cond_pcl->header.frame_id = "world";
  cond_pcl->header.stamp = ros::Time::now().toSec();
  pub_outlier_pcl.publish(cond_pcl);
  ros::Duration(0.1).sleep();
*/

  pcl::VoxelGrid<pcl::PointXYZ> vox;
  vox.setLeafSize (0.01f, 0.01f, 0.01f); // 0.01f for bunny
  vox.setInputCloud (cropped_pcl);
  pcl::PointCloud<pcl::PointXYZ>::Ptr vox_pcl(new pcl::PointCloud<pcl::PointXYZ>());
  vox.filter(*vox_pcl);
  ROS_ERROR_STREAM("published voxed pcl");

  vox_pcl->header.frame_id = "world";
  vox_pcl->header.stamp = ros::Time::now().toSec();
  pub_vox_pcl.publish(vox_pcl);
  ros::Duration(0.1).sleep();

  pcl::PointCloud<pcl::PointXYZ>::Ptr save_pcl;
  save_pcl = vox_pcl;
  pcl::io::savePCDFile(model_filename, *save_pcl, true);


  ros::Duration(1.0).sleep();
}


