/* Copyright 2015 Sanjiban Choudhury
 * filter_pcl.cpp
 *
 *  Created on: Jul 31, 2016
 *      Author: Sanjiban Choudhury
 */

#include <stdint.h>
#include <ros/ros.h>
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>


#define CROP
//#define VOX
//#define OUTLIER

ros::Publisher pub_pcl;

void PclCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &msg) {
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_pcl(new pcl::PointCloud<pcl::PointXYZ>(*msg));
#ifdef CROP
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cropped_pcl (new pcl::PointCloud<pcl::PointXYZ>(*filtered_pcl));
    pcl::CropBox<pcl::PointXYZ> crop;
    Eigen::Vector4f lb(-1e6, -1e6, 0, 1);
    Eigen::Vector4f ub(1e6, 1e6, 4, 1);
    crop.setMin(lb);
    crop.setMax(ub);
    crop.setInputCloud(cropped_pcl);
    crop.filter(*filtered_pcl);
  }
#endif

#ifdef VOX
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr vox_pcl (new pcl::PointCloud<pcl::PointXYZ>(*filtered_pcl));
    pcl::VoxelGrid<pcl::PointXYZ> vox;
    vox.setLeafSize (0.01f, 0.01f, 0.01f);
    vox.setInputCloud (vox_pcl);
    vox.filter(*filtered_pcl);
  }
#endif

#ifdef OUTLIER
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr outlier_pcl (new pcl::PointCloud<pcl::PointXYZ>(*filtered_pcl));
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> outlier;
    outlier.setMeanK(50);
    outlier.setStddevMulThresh (0.1);
    outlier.setInputCloud (outlier_pcl);
    outlier.filter (*filtered_pcl);
  }
#endif

  pub_pcl.publish(filtered_pcl);
}


int main(int argc, char **argv) {
  ros::init(argc, argv, "filter_pcl");
  ros::NodeHandle n("~");

  std::string input_topic, output_topic;
  ros_tools::getExpParam(input_topic,"input_topic");
  ros_tools::getExpParam(output_topic,"output_topic");

  ros::Subscriber sub_pcl = n.subscribe(input_topic, 1, &PclCallback ); // we want only latest message
  pub_pcl = n.advertise<pcl::PointCloud<pcl::PointXYZ> >(output_topic, 1);
  ros::Duration(0.1).sleep();

  ros::spin();
}


