/* Copyright 2015 Sanjiban Choudhury
 * io_utils.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning_experiments/io_utils.h"
#include <stdint.h>
#include "ig_active_reconstruction/view_space.hpp"
#include "ig_learning/state_utils.h"
#include <pcl_ros/transforms.h>

namespace iar = ig_active_reconstruction;
namespace su = ig_learning::state_utils;

namespace ig_learning {
namespace io_utils {

bool LoadActionSet(std::string filename, std::set<Action> &action_set) {
  iar::views::ViewSpace view_space;
  view_space.loadFromFile(filename);
  if (view_space.size() == 0)
    return false;
  unsigned int id = 0;
  for (iar::views::ViewSpace::Iterator it = view_space.begin(); it != view_space.end(); ++it, id++) {
    Action action;
    action.view = *it;
    action.id = id;
    action_set.insert(action);
  }
  return true;
}

/*
bool LoadPCDSet(std::string foldername, unsigned int num_pcd, pcl::PointCloud<pcl::PointXYZ> &cum_cloud) {
  for (unsigned i = 0; i < num_pcd; i++) {
     std::stringstream ss;
     ss << foldername << i << ".pcd";
     pcl::PointCloud<pcl::PointXYZ> cloud;
     if (pcl::io::loadPCDFile<pcl::PointXYZ> (ss.str(), cloud) == -1) {
       return false;
     }
     cum_cloud += cloud;
   }
  return true;
}
*/

bool LoadPCDSet(std::string foldername, std::string view_filename, pcl::PointCloud<pcl::PointXYZ> &cum_cloud) {
  iar::views::ViewSpace view_space;
  view_space.loadFromFile(view_filename);
  if (view_space.size() == 0)
    return false;

  unsigned int id = 0;
  for (iar::views::ViewSpace::Iterator it = view_space.begin(); it != view_space.end(); ++it, id++) {
    std::stringstream ss;
    ss << foldername << id << ".pcd";
    pcl::PointCloud<pcl::PointXYZ> cloud, transformed_cloud;
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (ss.str(), cloud) == -1) {
      return false;
    }
    pcl::transformPointCloud(cloud, transformed_cloud,
                             it->pose().position,
                             it->pose().orientation);
    cum_cloud += transformed_cloud;
  }

  return true;
}


bool LoadWorldMapSet(std::string foldername, unsigned int num_data,double model_res, std::vector<WorldMap> &world_map_set) {
  for (unsigned int i = 1; i <= num_data; i++) {
    std::string viewspace_file_path = foldername + "set" + std::to_string(i) + "/view_space.txt";
    std::string pcd_folder_path = foldername + "set" + std::to_string(i) + "/pcd/";
    std::string model_filename = foldername + "set" + std::to_string(i) + "/model.pcd";
    WorldMap world_map;
    if(!su::LoadWorldMap(viewspace_file_path, pcd_folder_path, model_filename, model_res, world_map))
      return false;
    world_map_set.push_back(world_map);
  }
  return true;
}

bool LoadPrecomputedExpert(std::string filename, std::vector< std::vector<unsigned int> > &expert_action_sequence) {
  std::ifstream infile(filename);
  if (!infile.good())
    return false;
  std::string line;
  while ( getline( infile, line ) ) {
     std::istringstream is( line );
     expert_action_sequence.push_back(
           std::vector<unsigned int>( std::istream_iterator<unsigned int>(is),
                             std::istream_iterator<unsigned int>() ) );
  }
  return true;
}


}
}

