cmake_minimum_required(VERSION 2.8.3)
project(ig_learning_experiments)
set(CMAKE_BUILD_TYPE RelWithDebInfo)

list( APPEND CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  cmake_modules
  flying_gazebo_stereo_cam
  gazebo_msgs
  geometry_msgs
  ig_learning
  roscpp
  sensor_msgs
  tf
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)
find_package(Eigen REQUIRED)
find_package(Boost REQUIRED)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
# generate_messages(
#   DEPENDENCIES
#   gazebo_msgs#   geometry_msgs#   sensor_msgs
# )

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES ig_learning_experiments
  CATKIN_DEPENDS flying_gazebo_stereo_cam gazebo_msgs geometry_msgs ig_learning roscpp sensor_msgs tf
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${Eigen_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
  STATIC
  src/io_utils.cpp
)
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})


## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(ig_learning_experiments ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
add_executable(${PROJECT_NAME}_create_pcd_database src/create_pcd_database.cpp)
target_link_libraries(${PROJECT_NAME}_create_pcd_database ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_test_state_transition src/test_state_transition.cpp)
target_link_libraries(${PROJECT_NAME}_test_state_transition ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_pcl_change_det src/pcl_change_det.cpp)
target_link_libraries(${PROJECT_NAME}_pcl_change_det ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_post_process_model_pcd src/post_process_model_pcd.cpp)
target_link_libraries(${PROJECT_NAME}_post_process_model_pcd ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_play_dataset src/play_dataset.cpp)
target_link_libraries(${PROJECT_NAME}_play_dataset ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_post_process_sampled_pcd src/post_process_sampled_pcd.cpp)
target_link_libraries(${PROJECT_NAME}_post_process_sampled_pcd ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_example_forward_training examples/example_forward_training.cpp)
target_link_libraries(${PROJECT_NAME}_example_forward_training ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_test_forward_training src/test_forward_training.cpp)
target_link_libraries(${PROJECT_NAME}_test_forward_training ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_example_test_online_policy examples/example_test_online_policy.cpp)
target_link_libraries(${PROJECT_NAME}_example_test_online_policy ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_stream_to_pcd_pose src/stream_to_pcd_pose.cpp)
target_link_libraries(${PROJECT_NAME}_stream_to_pcd_pose ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_spin_around_camera src/spin_around_camera.cpp)
target_link_libraries(${PROJECT_NAME}_spin_around_camera ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_filter_pcl src/filter_pcl.cpp)
target_link_libraries(${PROJECT_NAME}_filter_pcl ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_play_kinect_data src/play_kinect_data.cpp)
target_link_libraries(${PROJECT_NAME}_play_kinect_data ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_precompute_expert_on_dataset src/precompute_expert_on_dataset.cpp)
target_link_libraries(${PROJECT_NAME}_precompute_expert_on_dataset ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_scratch src/scratch.cpp)
target_link_libraries(${PROJECT_NAME}_scratch ${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_example_aggrevate examples/example_aggrevate.cpp)
target_link_libraries(${PROJECT_NAME}_example_aggrevate ${PROJECT_NAME} ${catkin_LIBRARIES})


## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(ig_learning_experiments_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
# target_link_libraries(ig_learning_experiments_node
#   ${catkin_LIBRARIES}
# )

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS ig_learning_experiments ig_learning_experiments_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_ig_learning_experiments.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
