clc;
clear;
close all;

%% Options
main_folder = '/home/aeroscout/data/information_planning/synthetic_datasets_gazebo/warehouse_dataset';
problem = 'coverage_with_motion';
% heuristics = {'average_entropy', ...
%                     'rear_entropy'};
                
heuristics = {'average_entropy_motion', ...
'rear_entropy_motion'};
predictors = {'predictor1'};

%% plot
figure;
counter = 2;
legend_str = {};
for i = 1:length(predictors)
    filename = strcat(main_folder,'/learnt_predictors/', problem,'/',predictors{i},'/results.txt');
    objective_trajectory_set = dlmread(filename);
    plot_objective_trajectory_stats(objective_trajectory_set, counter);
    counter = counter + 1;
    legend_str{length(legend_str)+1} = predictors{i};
end

for i = 1:length(heuristics)
    filename = strcat(main_folder,'/heuristic_results/',problem,'/',heuristics{i},'.txt');
    objective_trajectory_set = dlmread(filename);
    plot_objective_trajectory_stats(objective_trajectory_set, counter);
    counter = counter + 1;
    legend_str{length(legend_str)+1} = heuristics{i};
end

legend(legend_str);