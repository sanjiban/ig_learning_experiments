clc;
clear;
close all;

world_folder = '/home/aeroscout/data/information_planning/synthetic_datasets_gazebo/office_desk_dataset/worlds/';

lb = [-4 -4 0.5];%[-10 -10 0.2];
ub = [4 4 1.5];
is_valid = @(p) norm(p(1:2)) > 1;
num_vert = 50;

for i = 1:80
    folder = strcat(world_folder, 'world', num2str(i));
    filename = strcat(folder,'/view_space.txt');
    dlmwrite(filename ,num_vert);
    view_space = get_viewspace_bunny( lb, ub, is_valid, num_vert );
    dlmwrite(filename,view_space, '-append', 'delimiter', ' ');
    i
    pause(0.1);
end
