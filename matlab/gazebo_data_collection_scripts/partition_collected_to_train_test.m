clc;
close all;
clear;


main_folder = '/home/aeroscout/data/information_planning/synthetic_datasets_gazebo/warehouse_dataset';

train_folder = strcat(main_folder, '/train');
test_folder = strcat(main_folder, '/test');

train_count = 1;
for i = 1:10
    world_folder = strcat(main_folder, '/worlds/world', num2str(i));
    A = dlmread(strcat(world_folder, '/view_space.txt'));
    num_tot_views = A(1,1);
    total_view_space = A(2:end,:);
    model_file = strcat(world_folder, '/model.pcd');
    pcd_counter = 0;
    for j = 1:10
        view_space = total_view_space((1 + (j-1)*100):(j*100), :);
        num_views = num_tot_views / 10;
        
        % Lets create the set folder
        set_folder = strcat(train_folder, '/set', num2str(train_count));
        mkdir(set_folder);
        
        % Lets write the view space
        view_space_filename = strcat(set_folder, '/view_space.txt');
        dlmwrite(view_space_filename, num_views);
        dlmwrite(view_space_filename, view_space, '-append', 'delimiter', ' ');
        
        % Copy model
        new_model_file = strcat(set_folder, '/model.pcd');
        copyfile(model_file, new_model_file);
        
        % Copy pcd
        pcd_folder = strcat(set_folder, '/pcd');
        mkdir(pcd_folder);
        for k = 1:num_views
            copyfile(strcat(world_folder,'/pcd/', num2str(pcd_counter), '.pcd'), ...
                     strcat(pcd_folder, '/', num2str(k-1), '.pcd'));
            pcd_counter = pcd_counter + 1;
        end
        train_count = train_count + 1;
    end
end

