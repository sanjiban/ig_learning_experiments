function view_space = get_viewspace_bunny( lb, ub, is_valid, num_vert )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

view_space = [];
while length(view_space) < num_vert
    pos = rand(1,3).*(ub - lb) + lb;
    if (~is_valid(pos))
        continue;
    end
    
    dir1 = [0 0 1];
    dir2 = -normr([pos(1) pos(2) 0]);
    R = rot_vec( dir1, dir2 );
    q  = quaternion.rotationmatrix(R);
%     
%     q  = quaternion.angleaxis(0, dir);
%     q
    %ang = EulerAngles(q,'123')';
    %ang = ang + (pi/6)*(rand(1,3) - 0.5);
    %q = quaternion.eulerangles('123',ang(1), ang(2), ang(3));
    view_space = [view_space; pos vector(q)' real(q)];
end

end

