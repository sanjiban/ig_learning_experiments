clc;
clear;
close all;

main_folder = '/home/aeroscout/data/information_planning/tum_dataset/';
rawdump_folder = strcat(main_folder, 'raw_dump/');
num_rawdump = 161;

test_folder = strcat(main_folder, 'test/');
num_test = 10;
num_vert = 50;

%% Load rawdump pose
pose_set = [];
for i = 1:num_rawdump
    filename = strcat(rawdump_folder,num2str(i),'.pose');
    pose = dlmread(filename);   
    pose_set = [pose_set; pose];
end

scatter3(pose_set(:,1), pose_set(:,2), pose_set(:,3))

%% Create uber viewspace
uber_viewspace = strcat(rawdump_folder,'view_space.txt');
dlmwrite(uber_viewspace, num_rawdump);
dlmwrite(uber_viewspace,pose_set, '-append', 'delimiter', ' ');

%% Create test
for i = 1:num_test
    folder = strcat(test_folder,'set',num2str(i));
    filename = strcat(folder,'/view_space.txt');
    mkdir(folder);
    mkdir(strcat(folder,'/pcd'));
    dlmwrite(filename,num_vert);
    idx = randperm(size(pose_set,1));
    view_space = pose_set(idx(1:num_vert), :);
    dlmwrite(filename,view_space, '-append', 'delimiter', ' ');
    for j = 1:num_vert
        pcd_id = idx(j);
        pcd_filename = strcat(rawdump_folder,num2str(pcd_id),'.pcd');
        pcd_newfilename = strcat(folder,'/pcd/',num2str(j-1),'.pcd');
        copyfile(pcd_filename, pcd_newfilename);
    end
end