function view_space = get_viewspace( lb, ub, is_valid, num_vert )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

dir1 = [0 0 1];
dir2 = [1 0 0];
R = rot_vec( dir1, dir2 );
q  = quaternion.rotationmatrix(R);
view_space = [0 0 0 vector(q)' real(q)];
while length(view_space) < (num_vert-1)
    pos = rand(1,3).*(ub - lb) + lb;
    if (~is_valid(pos))
        continue;
    end
    
    dir1 = [0 0 1];
    yaw = rand()*2*pi;
    dir2 = [cos(yaw) sin(yaw) 0];
    R = rot_vec( dir1, dir2 );
    q  = quaternion.rotationmatrix(R);
%     
%     q  = quaternion.angleaxis(0, dir);
%     q
    %ang = EulerAngles(q,'123')';
    %ang = ang + (pi/6)*(rand(1,3) - 0.5);
    %q = quaternion.eulerangles('123',ang(1), ang(2), ang(3));
    view_space = [view_space; pos vector(q)' real(q)];
end

end

