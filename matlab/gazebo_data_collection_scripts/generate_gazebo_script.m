clc;
close all;
clear;


main_folder = '/home/aeroscout/data/information_planning/synthetic_datasets_gazebo/office_desk_dataset';
num_worlds = 80;
sensor = 'flying_kinect';
sensor_topic = '/camera/depth/points';

fid = fopen('/home/aeroscout/ros/informative_path_planning/src/ig_learning_experiments/scripts/collect_dataset_from_gazebo.sh', 'w');
fprintf(fid, '#!/bin/bash \n\n');

for i = 1:num_worlds
    pcd_folder = strcat(main_folder,'/worlds/world',num2str(i),'/pcd/');
    viewspace_file_path = strcat(main_folder,'/worlds/world',num2str(i),'/view_space.txt'); 
    model = strcat(main_folder,'/worlds/world',num2str(i),'.sdf');
    fprintf(fid, 'roslaunch ig_learning_experiments collect_dataset_from_gazebo.launch viewspace_file_path:=%s pcd_folder:=%s model:=%s sensor:=%s sensor_topic:=%s\n', ... 
        viewspace_file_path, pcd_folder, model, sensor, sensor_topic);
    pcd_model = strcat(main_folder,'/worlds/world',num2str(i),'/model.pcd'); 
    fprintf(fid, 'roslaunch ig_learning_experiments create_model_from_dataset.launch viewspace_file_path:=%s pcd_folder:=%s model_filename:=%s\n', ... 
        viewspace_file_path, pcd_folder, pcd_model);
end

fclose(fid);


