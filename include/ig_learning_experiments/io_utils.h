/* Copyright 2015 Sanjiban Choudhury
 * io_utils.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_EXPERIMENTS_INCLUDE_IG_LEARNING_EXPERIMENTS_IO_UTILS_H_
#define IG_LEARNING_EXPERIMENTS_INCLUDE_IG_LEARNING_EXPERIMENTS_IO_UTILS_H_

#include "ig_learning/state.h"

namespace ig_learning {
namespace io_utils {

bool LoadPCDSet(std::string foldername, std::string view_filename, pcl::PointCloud<pcl::PointXYZ> &cum_cloud);

bool LoadActionSet(std::string filename, std::set<Action> &action_set);

bool LoadWorldMapSet(std::string foldername, unsigned int num_data,
                     double model_res,
                     std::vector<WorldMap> &world_map_set);

bool LoadPrecomputedExpert(std::string filename, std::vector< std::vector<unsigned int> > &expert_action_sequence);
}
}


#endif /* IG_LEARNING_EXPERIMENTS_INCLUDE_IG_LEARNING_EXPERIMENTS_IO_UTILS_H_ */
